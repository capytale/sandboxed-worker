self.importScripts(
  "https://cdn.jsdelivr.net/npm/comlink@4.4.1/dist/umd/comlink.min.js"
);

class API {
  eval(code) {
    return self.eval(code);
  }
}

const api = new API();
Comlink.expose(api);
