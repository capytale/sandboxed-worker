import { wrap } from "https://cdn.jsdelivr.net/npm/comlink@4.4.1/+esm";

const workerURL = new URL("./worker.js", window.location.href).href;

// worker created with a 'data' url scheme has opaque origin
// https://html.spec.whatwg.org/multipage/workers.html#script-settings-for-workers:concept-origin-opaque
const dataURL =
  "data:text/javascript;base64," + btoa(`importScripts("${workerURL}");`);
const worker = new Worker(dataURL);
const api = wrap(worker);

(async () => {
  const workerOrigin = await api.eval("self.origin.toString()");
  console.log(`Worker origin: ${workerOrigin}`);

  console.log(`Computation by worker: 1 + 1 = ${await api.eval("1 + 1")}`);
})();
