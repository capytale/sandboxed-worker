# Sandboxed Web Worker

This repo demonstrates how to execute untrusted code in a separated thread in a secure way.

A demo [can be found here](https://capytale.forge.apps.education.fr/sandboxed-worker).

## Principle

We use [Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)
started in a [sandboxed iframe](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe#sandbox).
This ensures that HTTP requests do not come from the same origin.

## Implementation

We use vanilla vanilla Javascript.
